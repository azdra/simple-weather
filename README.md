# Simple Weather

### Technologies used:
<ul>
    <li>FrameWork JS: VueJS</li>
    <li>SCSS</li>
    <li>ESlint</li>
</ul>

## Installation
```
1 > git clone https://gitlab.com/baptiste.brand/simplon-weather.git
2 > cd simple-weather
3 > npm install 
4 > npm run serve
```

### Preview
![img.png](img.png)
![img_1.png](img_1.png)
